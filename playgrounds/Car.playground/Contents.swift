enum Direction: String {
    case standStill = "Standing still"
    case forward = "Move forward"
    case back = "Move back"
    case right = "Move right"
    case left = "Move left"
}

class Car {
    var model: String
    private(set) var moving = Go() {
        didSet {
            if (moving.direction != .standStill) && (moving.distance == 0.0) {
                moving.direction = .standStill
            }
        }
    }
    
    init(_ model: String, beginMoving: Go = Go()) {
        self.model = model
        self.executeCarMoving(to: beginMoving)
    }
    
    func executeCarMoving(to: Go) {
        self.moving = to
    }
}

struct Go {
    var direction: Direction
    var distance: Double
    
    init(direction: Direction, distance: Double) {
        self.direction = direction
        self.distance = distance
    }
    
    init() {
        self.init(direction: .standStill, distance: 0.0)
    }
}

var simpleCar = Car("Lada Priora")
print(simpleCar.moving.direction.rawValue)

var oldCar = Car("Mustang", beginMoving: Go(direction: .left, distance: 0))
print(oldCar.moving.direction.rawValue) //остается на месте, так как расстояние равно 0

oldCar.executeCarMoving(to: Go(direction: .back, distance: 51))
print(oldCar.moving.direction.rawValue, "to distance:", oldCar.moving.distance)
