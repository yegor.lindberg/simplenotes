func calcLetters(str: String) -> [String: Int] {
    var lettersDictionary = [String: Int]()
    for ch in str {
        if let lettersAmount = lettersDictionary[String(ch)] {
            lettersDictionary[String(ch)] = lettersAmount + 1
        } else {
           lettersDictionary[String(ch)] = 1
        }
    }
    
    return lettersDictionary
}

print(calcLetters(str: "hello world"), "\n")
print(calcLetters(str: "abcdefghijklmnopqrstuvwxyz"), "\n")
