import Foundation

extension Array {
    //меняем текущий массив
    mutating func randomShuffle() {
        if self.count > 2 {
            self.shuffleThis()
        } else {
            print("Impossible make the operation(random shuffle): Array have 2 elements or less.")
        }
    }
    
    private mutating func shuffleThis() {
        var amountShuffleMoving = Double(self.count) / 2
        amountShuffleMoving.round(.up)
        for _ in 0..<Int(amountShuffleMoving) {
            var firstElement = Int.random(in: 0..<self.count)
            let secondElement = Int.random(in: 0..<self.count)
            if firstElement == secondElement {
                firstElement = (firstElement == self.count - 1) ? (firstElement - 1) : (firstElement + 1)
            }
            self.swapAt(firstElement, secondElement)
            print(self)
        }
        print("---")
    }
}

var errArr = [1, 2]
var arr = [1, 2, 3]
var arr1 = [1, 2, 3, 4, 5]

errArr.randomShuffle()
arr.randomShuffle()
arr1.randomShuffle()
