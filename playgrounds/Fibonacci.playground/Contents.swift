func fibonachi(n: Int) -> Int {
    var previousVal = 0
    var currentVal = 1
    var fibonachiNum = 0
    let neededIndex = n > 0 ? n : (n * -1)
    
    for _ in 0..<neededIndex {
        previousVal = currentVal
        currentVal = fibonachiNum
        fibonachiNum = previousVal + currentVal
    }
    
    if n > 0 {
        fibonachiNum = fibonachiNum * -1
    }
    
    return fibonachiNum
}

print(fibonachi(n: 4))
