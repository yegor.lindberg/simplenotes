//
//  NoteExtension.swift
//  SimpleNote
//
//  Created by Yegor Lindberg on 30/06/2019.
//  Copyright © 2019 Yegor Lindberg. All rights reserved.
//

import UIKit


extension Note {
    var json: [String: Any] {
        var jsonTemp: [String: Any] = [
            "uid" : uid,
            "title" : title,
            "content" : content,
        ]
        if color != .white {
            jsonTemp["color"] = self.colorToString()
        }
        if importance != .usual {
            jsonTemp["importance"] = importance.rawValue
        }
        if selfDestructionDate != nil {
            jsonTemp["selfDestructionDate"] = Int(selfDestructionDate!.timeIntervalSince1970)
        }
        return jsonTemp
    }
    
    static func parse(json: [String: Any]) -> Note? {
        guard let title      = json["title"] as? String,
              let content    = json["content"] as? String,
              let importance = json["importance"] as? Int else {
            return nil
        }
        var dateIsEmpty = true
        var timeStamp: Int = 0
        if let jsonTimeStamp = json["selfDestructionDate"] as? Int {
            dateIsEmpty = false
            timeStamp = jsonTimeStamp
        }
        return Note(uniqueId: json["uid"] as? String ?? UUID().uuidString,
                    title: title,
                    content: content,
                    rgbaStringToColor(from: json["color"] as? String ?? "") ?? UIColor.white,
                    importance: Importance(rawValue: importance) ?? .usual,
                    selfDestructionDate: dateIsEmpty ? nil : Date(timeIntervalSince1970: TimeInterval(timeStamp)))
    }
    
    func colorToString() -> String {
        let rgba = self.color.rgba
        return "\(rgba.red),\(rgba.green),\(rgba.blue),\(rgba.alpha)"
    }
    
    static func rgbaStringToColor(from str: String) -> UIColor? {
        let rgbaArr = str.components(separatedBy: ",")
        guard rgbaArr.count == 4 else { return nil }
        guard let red   = Double(rgbaArr[0]) else { return nil }
        guard let green = Double(rgbaArr[1]) else { return nil }
        guard let blue  = Double(rgbaArr[2]) else { return nil }
        guard let alpha = Double(rgbaArr[3]) else { return nil }
        return UIColor(red: CGFloat(red), green: CGFloat(green), blue: CGFloat(blue), alpha: CGFloat(alpha))
    }
    
}
