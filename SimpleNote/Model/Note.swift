//
//  Note.swift
//  SimpleNote
//
//  22/06/2019.
//

import UIKit


enum Importance: Int {
    case unimportant = 1
    case usual = 2
    case important = 3
}

struct Note {
    let uid       : String
    let title     : String
    let content   : String
    let color     : UIColor
    let importance: Importance
    let selfDestructionDate: Date?
    
    init(uniqueId  : String = UUID().uuidString,
         title     : String,
         content   : String,
         _ color   : UIColor = UIColor.white,
         importance: Importance,
         selfDestructionDate: Date? = nil)
    {
        self.uid = uniqueId
        self.title = title
        self.content = content
        self.color = color
        self.importance = importance
        self.selfDestructionDate = selfDestructionDate
    }
    
}

