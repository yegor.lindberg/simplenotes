//
//  FileNotebook.swift
//  SimpleNote
//
//  Created by Yegor Lindberg on 01/07/2019.
//  Copyright © 2019 Yegor Lindberg. All rights reserved.
//

import Foundation


class FileNotebook {
    private(set) var notes = [Note]()
    
    enum DecodingError: Error {
        case missingFile
        case parsingError
    }
    
    init(fileName: String) throws {
        _ = try self.loadFromFile(fileName: fileName)
    }
    
    init() {}
    
    public func add(_ note: Note) {
        if !self.notes.contains(where: { $0.uid == note.uid }) {
            self.notes.append(note)
        }
    }

    public func remove(with uid: String) {
        guard let index = notes.firstIndex(where: {$0.uid == uid}) else {
            print("Element with this UID not found.")
            return
        }
        self.notes.remove(at: index)
    }
    
    public func clearNotesList() {
        self.notes.removeAll()
    }
    
    public func saveToFile(fileName: String) throws -> Bool {
        let fileManager = FileManager.default
        var isDirectory: ObjCBool = false
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: self.getJsonData(), options: .prettyPrinted)
            let url = try fileManager.url(for: .cachesDirectory,
                                                  in: .userDomainMask,
                                                  appropriateFor: nil,
                                                  create: false)
            let fileNameWithExtension = fileName + ".json"
            let jsonUrl = url.appendingPathComponent(fileNameWithExtension)
            if !fileManager.fileExists(atPath: jsonUrl.path, isDirectory: &isDirectory) {
                fileManager.createFile(atPath: jsonUrl.path, contents: nil, attributes: nil)
            }
            try jsonData.write(to: jsonUrl)
        } catch {
            print("Error: \(error).")
            return false
        }
        return true
    }
    
    public func loadFromFile(fileName: String) throws -> Bool {
        do {
            let url = try FileManager.default.url(for: .cachesDirectory,
                                                  in: .userDomainMask,
                                                  appropriateFor: nil,
                                                  create: false)
            let fileNameWithExtension = fileName + ".json"
            let jsonUrl = url.appendingPathComponent(fileNameWithExtension)
            let jsonReadData = NSData(contentsOf: jsonUrl)
            if let jsonReadData = jsonReadData {
                let parsedNotes = try JSONSerialization.jsonObject(with: jsonReadData as Data,
                                                                   options: .mutableContainers)
                guard let readedNotes = parsedNotes as? [[String: Any]] else {
                    throw DecodingError.parsingError
                }
                for readedNote in readedNotes {
                    guard let note = Note.parse(json: readedNote) else { continue }
                    self.add(note)
                }
            }
        } catch {
            print("Error: \(error).")
            return false
        }
        return true
    }
    
    private func getJsonData() -> [AnyObject] {
        var notesInJson = [AnyObject]()
        for note in self.notes {
            notesInJson.append(note.json as AnyObject)
        }
        return notesInJson
    }
    
    
}
