//
//  ViewController.swift
//  SimpleNote
//
//  Created by Yegor Lindberg on 22/06/2019.
//  Copyright © 2019 Yegor Lindberg. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet var viewColor: UIView!
    var colorNum = 1
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBAction func onSetColorButtonTouch(_ sender: UIButton) {
        switch colorNum {
        case 0:
            let rgba = UIColor.white.rgba
            print(rgba.red, "- red")
            print(rgba.green, "- green")
            print(rgba.blue, "- blue")
            print(rgba.alpha, "- alpha\n")
            self.viewColor.backgroundColor = UIColor(red: rgba.red, green: rgba.green, blue: rgba.blue, alpha: rgba.alpha)
            self.colorNum = 1
        case 1:
            let rgba = UIColor.yellow.rgba
            print(rgba.red, "- red")
            print(rgba.green, "- green")
            print(rgba.blue, "- blue")
            print(rgba.alpha, "- alpha\n")
            self.viewColor.backgroundColor = UIColor(red: rgba.red, green: rgba.green, blue: rgba.blue, alpha: rgba.alpha)
            self.colorNum = 0
        default:
            break
        }
        
    }
}

